request = require 'request'

URLS =
  production:
    getVerifiedStatus: 'https://svcs.paypal.com/AdaptiveAccounts/GetVerifiedStatus'
  development:
    getVerifiedStatus: 'https://svcs.sandbox.paypal.com/AdaptiveAccounts/GetVerifiedStatus'


class NodePaypal

  constructor: (options) ->
    @options = options
    @options.env = if options.env is 'production' then 'production' else 'development'
    @URLS = URLS[@options.env]
    @requestOptions =
      headers:
        'X-PAYPAL-SECURITY-USERID': options.userId
        'X-PAYPAL-SECURITY-PASSWORD': options.password
        'X-PAYPAL-SECURITY-SIGNATURE': options.signature
        'X-PAYPAL-APPLICATION-ID': options.appId
        'X-PAYPAL-RESPONSE-DATA-FORMAT': 'JSON'
        'X-PAYPAL-REQUEST-DATA-FORMAT': 'JSON'
      json: true

  getVerifiedStatus: (data, cb) ->
    payload = @requestOptions
    payload.body =
      emailAddress: data.email
      firstName: data.firstName
      lastName: data.lastName
      matchCriteria: if @options.env is 'production' then 'NAME' else 'NONE' # name doesn't work in sandbox
      requestEnvelope:
        detailLevel: 'ReturnAll'
        errorLanguage: 'en_US'

    request.post @URLS.getVerifiedStatus, payload, cb 


module.exports = (options) ->
 return new NodePaypal options
