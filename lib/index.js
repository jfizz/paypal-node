var NodePaypal, URLS, request;

request = require('request');

URLS = {
  production: {
    getVerifiedStatus: 'https://svcs.paypal.com/AdaptiveAccounts/GetVerifiedStatus'
  },
  development: {
    getVerifiedStatus: 'https://svcs.sandbox.paypal.com/AdaptiveAccounts/GetVerifiedStatus'
  }
};

NodePaypal = (function() {
  function NodePaypal(options) {
    this.options = options;
    this.options.env = options.env === 'production' ? 'production' : 'development';
    this.URLS = URLS[this.options.env];
    this.requestOptions = {
      headers: {
        'X-PAYPAL-SECURITY-USERID': options.userId,
        'X-PAYPAL-SECURITY-PASSWORD': options.password,
        'X-PAYPAL-SECURITY-SIGNATURE': options.signature,
        'X-PAYPAL-APPLICATION-ID': options.appId,
        'X-PAYPAL-RESPONSE-DATA-FORMAT': 'JSON',
        'X-PAYPAL-REQUEST-DATA-FORMAT': 'JSON'
      },
      json: true
    };
  }

  NodePaypal.prototype.getVerifiedStatus = function(data, cb) {
    var payload;
    payload = this.requestOptions;
    payload.body = {
      emailAddress: data.email,
      firstName: data.firstName,
      lastName: data.lastName,
      matchCriteria: this.options.env === 'production' ? 'NAME' : 'NONE',
      requestEnvelope: {
        detailLevel: 'ReturnAll',
        errorLanguage: 'en_US'
      }
    };
    return request.post(this.URLS.getVerifiedStatus, payload, cb);
  };

  return NodePaypal;

})();

module.exports = function(options) {
  return new NodePaypal(options);
};
