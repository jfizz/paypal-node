gulp = require 'gulp'
coffee = require 'gulp-coffee'
gutil = require 'gulp-util'


input =
  coffee: "#{__dirname}/src/**/*.coffee"

output =
  js: "#{__dirname}/lib"

gulp.task 'coffee', ->
  gulp.src input.coffee
  .pipe coffee({bare: true}).on 'error', gutil.log
  .pipe gulp.dest(output.js)
